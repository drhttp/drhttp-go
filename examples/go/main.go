package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"

	moesifmiddleware "bitbucket.org/drhttp/moesifmiddleware-go"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello from  go!")
}

func main() {
	dsn, _ := url.Parse(os.Getenv("DRHTTP_DSN"))
	var moesifOptions = map[string]interface{}{
		"Application_Id": dsn.User.String(),
	}

	http.Handle("/", moesifmiddleware.MoesifMiddleware(http.HandlerFunc(handler), moesifOptions))

	log.Println("listening on 80")
	log.Fatal(http.ListenAndServe(":80", nil))
}
