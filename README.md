This is the official [Golang](https://golang.org/) client for [Dr. Ashtetepe](https://drhttp.com/) service.

*Note: At the moment, as we are developing Dr. Ashtetepe, we ask you to use [Moesif](https://www.moesif.com/)'s [Go library](https://github.com/pomarec/moesifmiddleware-go). Thanks to them to permit that via their [Apache licence](https://raw.githubusercontent.com/Moesif/moesifmiddleware-go/master/LICENSE)*. This README is a [TL;DR](https://en.wiktionary.org/wiki/tl;dr) for Dr.Ashtetepe usage.

# Installation
> [An integration example is provided here](https://bitbucket.org/drhttp/drhttp-go/src/master/examples/go/)

1) Install go package *(we use a fork of moesif where drhttp url is hardcoded)*
    ```
    go get bitbucket.org/drhttp/moesifmiddleware-go
    ```

2) Retrieve a `dsn` ([Data source name](https://en.wikipedia.org/wiki/Data_source_name)) which can be found in [your project](https://drhttp.com/projects). eg: `https://<my_project_api_key>@api.drhttp.com/`

3) Setup the middleware :
    ```go
    import (
        ...

        moesifmiddleware "bitbucket.org/drhttp/moesifmiddleware-go"
    )

    func handler(w http.ResponseWriter, r *http.Request) {
        ...
    }

    func main() {
        var moesifOptions = map[string]interface{}{
            "Application_Id": "<my_project_api_key>",
        }
        http.Handle("/", moesifmiddleware.MoesifMiddleware(http.HandlerFunc(handler), moesifOptions))

        ...
    }
    ```

### User identification

See [identifiUser in Moesif documentation](https://github.com/Moesif/moesifmiddleware-go#identify_user)

### Device identification

*Note: Device identification is not available yet in the go library*

## Outbound request recording

*Note: Outbound request recording is not available yet in the go library*

# Troubleshooting

Please [report any issue](https://bitbucket.org/drhttp/drhttp-go/issues/new) you encounter concerning documentation or implementation. This is very much appreciated. We'll upstream the improvements to Moesif.
